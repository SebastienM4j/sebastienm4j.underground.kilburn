﻿BEGIN;

DELETE FROM operation_operation_source;
DELETE FROM operation_tag;
						
DELETE FROM operation;
ALTER SEQUENCE operation_id_seq RESTART WITH 1;

COMMIT;