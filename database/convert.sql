﻿select * from operation_source

select id, data::json #>> '{dateOperation}' as dateOperation
	from operation_source

select (extract(epoch from timestamp '2017-03-01T00:00:00')-3600)*1000
select (extract(epoch from '2017-03-01T00:00:00'::timestamp)-3600)*1000

select regexp_replace(data, '201[6|7]-[0-9]{2}-[0-9]{2}T00:00:00', 'ZZZ') from operation_source;

CREATE OR REPLACE FUNCTION convertOperationSourceDate() RETURNS VOID AS $$
    DECLARE
        op operation_source%ROWTYPE;
        dateToConvert VARCHAR;
	dateMonCmb bigint;
	newData VARCHAR;
    BEGIN
	FOR op in select * from operation_source LOOP
		dateToConvert = (select op.data::json #>> '{dateOperation}');
		dateMonCmb = (select (extract(epoch from dateToConvert::timestamp)-3600)*1000);
		newData = (select regexp_replace(op.data, '201[6|7]-[0-9]{2}-[0-9]{2}T00:00:00', dateMonCmb::varchar));
		
		--RAISE INFO 'op % date % cmb % = %', op.id, dateToConvert, dateMonCmb, newData;

		update operation_source set data = newData where id = op.id;
	END LOOP;
    END;
$$ LANGUAGE 'plpgsql';

select convertOperationSourceDate()