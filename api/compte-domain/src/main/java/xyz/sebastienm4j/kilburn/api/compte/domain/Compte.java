package xyz.sebastienm4j.kilburn.api.compte.domain;

import java.util.Set;

/**
 * Représente un compte bancaire.
 */
public interface Compte
{
    public static enum Type
    {
        CHEQUE,
        CEL,
        PEL,
        LIVRET_A
    }

    Integer getId();

    void setId(Integer id);

    Type getType();

    void setType(Type type);

    String getLibelle();

    void setLibelle(String libelle);

    String getNumero();

    void setNumero(String numero);

    String getIban();

    void setIban(String iban);

    String getRibBanque();

    void setRibBanque(String ribBanque);

    String getRibGuichet();

    void setRibGuichet(String ribGuichet);

    String getRibCompte();

    void setRibCompte(String ribCompte);

    String getRibCle();

    void setRibCle(String ribCle);

    String getBic();

    void setBic(String bic);

    Set<String> getTags();

    void setTags(Set<String> tags);

}
