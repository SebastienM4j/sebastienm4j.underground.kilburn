package xyz.sebastienm4j.kilburn.api.operation.domain;

import java.time.Instant;

/**
 * Représente une opération bancaire, ou plutôt les données
 * source d'une opération. Elle est issue d'un import depuis
 * une source de données.
 */
public interface OperationSource
{

    Long getId();

    void setId(Long id);


    Instant getDateImport();

    void setDateImport(Instant dateImport);

    String getDatasource();

    void setDatasource(String datasource);

    String getCompte();

    void setCompte(String compte);

    String getData();

    void setData(String data);

}
