package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MonCmbSyntheseComptes
{
    @JsonProperty("listComptePref")
    private List<MonCmbComptePref> listComptePref;
    @JsonProperty("listCompteTitulaireCotitulaire")
    private List<MonCmbCompteTitulaireCotitulaire> listCompteTitulaireCotitulaire;
    private Object listCompteMandataire;
    private Object listCompteLegalRep;
    private Double globalBalance;
    private Double transactionalAccountsBalance;
    private boolean capable;


    public List<MonCmbComptePref> getListComptePref()
    {
        return listComptePref;
    }

    public void setListComptePref(List<MonCmbComptePref> listComptePref)
    {
        this.listComptePref = listComptePref;
    }

    public List<MonCmbCompteTitulaireCotitulaire> getListCompteTitulaireCotitulaire()
    {
        return listCompteTitulaireCotitulaire;
    }

    public void setListCompteTitulaireCotitulaire(List<MonCmbCompteTitulaireCotitulaire> listCompteTitulaireCotitulaire)
    {
        this.listCompteTitulaireCotitulaire = listCompteTitulaireCotitulaire;
    }

    public Object getListCompteMandataire()
    {
        return listCompteMandataire;
    }

    public void setListCompteMandataire(Object listCompteMandataire)
    {
        this.listCompteMandataire = listCompteMandataire;
    }

    public Object getListCompteLegalRep()
    {
        return listCompteLegalRep;
    }

    public void setListCompteLegalRep(Object listCompteLegalRep)
    {
        this.listCompteLegalRep = listCompteLegalRep;
    }

    public Double getGlobalBalance()
    {
        return globalBalance;
    }

    public void setGlobalBalance(Double globalBalance)
    {
        this.globalBalance = globalBalance;
    }

    public Double getTransactionalAccountsBalance()
    {
        return transactionalAccountsBalance;
    }

    public void setTransactionalAccountsBalance(Double transactionalAccountsBalance)
    {
        this.transactionalAccountsBalance = transactionalAccountsBalance;
    }

    public boolean isCapable()
    {
        return capable;
    }

    public void setCapable(boolean capable)
    {
        this.capable = capable;
    }

}
