package xyz.sebastienm4j.kilburn.support.client.moncmb.service;

import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbAuthToken;
import xyz.sebastienm4j.kilburn.support.client.moncmb.exception.MonCmbAuthenticationException;

/**
 * Service d'authentification sur le site mon.cmb.fr.
 */
public interface MonCmbAuthenticationService
{

    /**
     * Authentification sur le site mon.cmb.fr
     * afin de récupérer des tokens d'accès à l'API REST.
     *
     * @param login Identifiant (non <tt>null</tt>, non vide)
     * @param password Mot de passe (non <tt>null</tt>, non vide)
     * @return Les tokens obtenus suite à l'authentification
     * @throws MonCmbAuthenticationException En cas d'échec de l'authentification
     */
    MonCmbAuthToken authenticate(String login, String password) throws MonCmbAuthenticationException;

}
