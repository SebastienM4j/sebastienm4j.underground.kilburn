package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbBaseOperation
{
    private String codeDevise;
    private String dateOperation;
    private String dateValeur;
    private String libelleCourt;
    private String libelleLong;
    private boolean marquage;
    private Double montantEnDevise;
    private Double montantEnEuro;
    private boolean pointage;

    public String getCodeDevise()
    {
        return codeDevise;
    }

    public void setCodeDevise(String codeDevise)
    {
        this.codeDevise = codeDevise;
    }

    public String getDateOperation()
    {
        return dateOperation;
    }

    public void setDateOperation(String dateOperation)
    {
        this.dateOperation = dateOperation;
    }

    public String getDateValeur()
    {
        return dateValeur;
    }

    public void setDateValeur(String dateValeur)
    {
        this.dateValeur = dateValeur;
    }

    public String getLibelleCourt()
    {
        return libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt)
    {
        this.libelleCourt = libelleCourt;
    }

    public String getLibelleLong()
    {
        return libelleLong;
    }

    public void setLibelleLong(String libelleLong)
    {
        this.libelleLong = libelleLong;
    }

    public boolean isMarquage()
    {
        return marquage;
    }

    public void setMarquage(boolean marquage)
    {
        this.marquage = marquage;
    }

    public Double getMontantEnDevise()
    {
        return montantEnDevise;
    }

    public void setMontantEnDevise(Double montantEnDevise)
    {
        this.montantEnDevise = montantEnDevise;
    }

    public Double getMontantEnEuro()
    {
        return montantEnEuro;
    }

    public void setMontantEnEuro(Double montantEnEuro)
    {
        this.montantEnEuro = montantEnEuro;
    }

    public boolean isPointage()
    {
        return pointage;
    }

    public void setPointage(boolean pointage)
    {
        this.pointage = pointage;
    }
}
