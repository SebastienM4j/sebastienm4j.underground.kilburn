package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

/**
 * Contient les informations d'authentifications obtenues
 * après la connexion sur le site mon.cmb.fr.
 */
public class MonCmbAuthToken
{
    private final String accessToken;
    private final String idToken;


    public MonCmbAuthToken(String accessToken, String idToken)
    {
        this.accessToken = accessToken;
        this.idToken = idToken;
    }


    public String getAccessToken()
    {
        return accessToken;
    }

    public String getIdToken()
    {
        return idToken;
    }

}
