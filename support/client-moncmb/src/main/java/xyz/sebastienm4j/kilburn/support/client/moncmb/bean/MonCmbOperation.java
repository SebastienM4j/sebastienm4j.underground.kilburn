package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbOperation extends MonCmbBaseOperation
{
    private boolean indicateurTempsReel;

    public boolean isIndicateurTempsReel()
    {
        return indicateurTempsReel;
    }

    public void setIndicateurTempsReel(boolean indicateurTempsReel)
    {
        this.indicateurTempsReel = indicateurTempsReel;
    }

}
