package xyz.sebastienm4j.kilburn.support.client.moncmb.exception;

/**
 * Exception lors de l'authentification sur le site mon.cmb.fr.
 */
public class MonCmbAuthenticationException extends Exception
{
    private static final long serialVersionUID = -9075642621813094829L;


    public MonCmbAuthenticationException()
    {
        super();
    }

    public MonCmbAuthenticationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MonCmbAuthenticationException(String message)
    {
        super(message);
    }

    public MonCmbAuthenticationException(Throwable cause)
    {
        super(cause);
    }

}
