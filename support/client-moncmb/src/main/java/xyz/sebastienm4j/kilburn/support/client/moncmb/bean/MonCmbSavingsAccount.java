package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbSavingsAccount
{
    private String index;
    private String libelleContrat;
    private String nomTitulaire;
    private Object scheduledPayments;
    private Boolean selectable;
    private Double solde;
    private String technicalIndex;


    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getLibelleContrat() {
        return libelleContrat;
    }

    public void setLibelleContrat(String libelleContrat) {
        this.libelleContrat = libelleContrat;
    }

    public String getNomTitulaire() {
        return nomTitulaire;
    }

    public void setNomTitulaire(String nomTitulaire) {
        this.nomTitulaire = nomTitulaire;
    }

    public Object getScheduledPayments() {
        return scheduledPayments;
    }

    public void setScheduledPayments(Object scheduledPayments) {
        this.scheduledPayments = scheduledPayments;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public String getTechnicalIndex() {
        return technicalIndex;
    }

    public void setTechnicalIndex(String technicalIndex) {
        this.technicalIndex = technicalIndex;
    }
}
