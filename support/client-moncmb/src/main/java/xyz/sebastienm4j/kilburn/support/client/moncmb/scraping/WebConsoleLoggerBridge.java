package xyz.sebastienm4j.kilburn.support.client.moncmb.scraping;

import com.gargoylesoftware.htmlunit.WebConsole;
import com.gargoylesoftware.htmlunit.WebConsole.Logger;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Permet de faire le pont entre la {@link WebConsole} et autre chose.
 * Le pont est réalisé par des liteners (de type {@link Consumer}) à qui
 * sont transmis les logs en fonction de leur niveau.
 */
public class WebConsoleLoggerBridge implements Logger
{
    private static enum ListenerType { TRACE, DEBUG, INFO, WARN, ERROR }


    private boolean traceEnabled;
    private boolean debugEnabled;
    private boolean infoEnabled;
    private boolean warnEnabled;
    private boolean errorEnabled;
    private Map<ListenerType, List<Consumer<String>>> listeners;


    public WebConsoleLoggerBridge()
    {
        this.listeners = new HashMap<>();
    }


    private void doLog(ListenerType type, String message)
    {
        List<Consumer<String>> list = listeners.get(type);
        if(!CollectionUtils.isEmpty(list)) {
            list.forEach(l -> l.accept(message));
        }
    }


    @Override
    public boolean isTraceEnabled()
    {
        return traceEnabled;
    }

    public void setTraceEnabled(boolean traceEnabled)
    {
        this.traceEnabled = traceEnabled;
    }

    public void addTraceListener(Consumer<String> listener)
    {
        Assert.notNull(listener, "Absence du listener");

        ListenerType type = ListenerType.TRACE;
        if(!listeners.containsKey(type)) {
            listeners.put(type, new ArrayList<>());
        }
        List<Consumer<String>> list = listeners.get(type);
        list.add(listener);
    }

    @Override
    public void trace(Object message)
    {
        doLog(ListenerType.TRACE, (String)message);
    }

    @Override
    public boolean isDebugEnabled()
    {
        return debugEnabled;
    }

    public void setDebugEnabled(boolean debugEnabled)
    {
        this.debugEnabled = debugEnabled;
    }

    public void addDebugListener(Consumer<String> listener)
    {
        Assert.notNull(listener, "Absence du listener");

        ListenerType type = ListenerType.DEBUG;
        if(!listeners.containsKey(type)) {
            listeners.put(type, new ArrayList<>());
        }
        List<Consumer<String>> list = listeners.get(type);
        list.add(listener);
    }

    @Override
    public void debug(Object message)
    {
        doLog(ListenerType.DEBUG, (String)message);
    }

    @Override
    public boolean isInfoEnabled()
    {
        return infoEnabled;
    }

    public void setInfoEnabled(boolean infoEnabled)
    {
        this.infoEnabled = infoEnabled;
    }

    public void addInfoListener(Consumer<String> listener)
    {
        Assert.notNull(listener, "Absence du listener");

        ListenerType type = ListenerType.INFO;
        if(!listeners.containsKey(type)) {
            listeners.put(type, new ArrayList<>());
        }
        List<Consumer<String>> list = listeners.get(type);
        list.add(listener);
    }

    @Override
    public void info(Object message)
    {
        doLog(ListenerType.INFO, (String)message);
    }

    @Override
    public boolean isWarnEnabled()
    {
        return warnEnabled;
    }

    public void setWarnEnabled(boolean warnEnabled)
    {
        this.warnEnabled = warnEnabled;
    }

    public void addWarnListener(Consumer<String> listener)
    {
        Assert.notNull(listener, "Absence du listener");

        ListenerType type = ListenerType.WARN;
        if(!listeners.containsKey(type)) {
            listeners.put(type, new ArrayList<>());
        }
        List<Consumer<String>> list = listeners.get(type);
        list.add(listener);
    }

    @Override
    public void warn(Object message)
    {
        doLog(ListenerType.WARN, (String)message);
    }

    @Override
    public boolean isErrorEnabled()
    {
        return errorEnabled;
    }

    public void setErrorEnabled(boolean errorEnabled)
    {
        this.errorEnabled = errorEnabled;
    }

    public void addErrorListener(Consumer<String> listener)
    {
        Assert.notNull(listener, "Absence du listener");

        ListenerType type = ListenerType.ERROR;
        if(!listeners.containsKey(type)) {
            listeners.put(type, new ArrayList<>());
        }
        List<Consumer<String>> list = listeners.get(type);
        list.add(listener);
    }

    @Override
    public void error(Object message)
    {
        doLog(ListenerType.ERROR, (String)message);
    }

}
