package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MonCmbSavingsProduct
{
    private Double ceiling;
    private String libelleProduit;
    private String nomTitulaire;
    @JsonProperty("savingsAccounts")
    private List<MonCmbSavingsAccount> savingsAccounts;
    private Double soldeProduit;


    public Double getCeiling() {
        return ceiling;
    }

    public void setCeiling(Double ceiling) {
        this.ceiling = ceiling;
    }

    public String getLibelleProduit() {
        return libelleProduit;
    }

    public void setLibelleProduit(String libelleProduit) {
        this.libelleProduit = libelleProduit;
    }

    public String getNomTitulaire() {
        return nomTitulaire;
    }

    public void setNomTitulaire(String nomTitulaire) {
        this.nomTitulaire = nomTitulaire;
    }

    public List<MonCmbSavingsAccount> getSavingsAccounts() {
        return savingsAccounts;
    }

    public void setSavingsAccounts(List<MonCmbSavingsAccount> savingsAccounts) {
        this.savingsAccounts = savingsAccounts;
    }

    public Double getSoldeProduit() {
        return soldeProduit;
    }

    public void setSoldeProduit(Double soldeProduit) {
        this.soldeProduit = soldeProduit;
    }
}
