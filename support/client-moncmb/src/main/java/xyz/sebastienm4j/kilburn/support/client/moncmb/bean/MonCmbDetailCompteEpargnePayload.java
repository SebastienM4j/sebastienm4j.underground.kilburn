package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbDetailCompteEpargnePayload
{
    private String index;
    private boolean callEtalis;
    private boolean domirama;


    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public boolean isCallEtalis() {
        return callEtalis;
    }

    public void setCallEtalis(boolean callEtalis) {
        this.callEtalis = callEtalis;
    }

    public boolean isDomirama() {
        return domirama;
    }

    public void setDomirama(boolean domirama) {
        this.domirama = domirama;
    }
}
