package xyz.sebastienm4j.kilburn.support.client.moncmb.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import xyz.sebastienm4j.kilburn.shared.rest.client.RestClientService;
import xyz.sebastienm4j.kilburn.shared.rest.client.RestClientUnexpectedResponseException;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.*;

import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.OK;

/**
 * Service permettant d'intérroger l'API "domiapi" du site mon.cmb.fr.
 */
@Service
public class MonCmbDomiApiClientService extends RestClientService
{

    @Override
    public String baseUrl()
    {
        return "https://mon.cmb.fr/domiapi/oauth/json";
    }


    public MonCmbInfosPerson infosPerson(MonCmbAuthToken token) throws RestClientUnexpectedResponseException
    {
        Assert.notNull(token, "Absence du token");

        RestTemplate template = restTemplate(OK);
        HttpEntity<String> request = new HttpEntity<>("{}", defaultHeaders(token));
        ResponseEntity<MonCmbInfosPerson> response = template.exchange(url("/edr/infosPerson"), POST, request, MonCmbInfosPerson.class);

        return response.getBody();
    }

    public MonCmbSyntheseComptes syntheseComptes(MonCmbAuthToken token, MonCmbTypeListeCompte typeListeCompte) throws RestClientUnexpectedResponseException
    {
        Assert.notNull(token, "Absence du token");
        Assert.notNull(typeListeCompte, "Absence de la liste des comptes");

        RestTemplate template = restTemplate(OK);
        HttpEntity<MonCmbTypeListeCompte> request = new HttpEntity<>(typeListeCompte, defaultHeaders(token));
        ResponseEntity<MonCmbSyntheseComptes> response = template.exchange(url("/accounts/synthesecomptes"), POST, request, MonCmbSyntheseComptes.class);

        return response.getBody();
    }

    public MonCmbDetailCompte detailCompte(MonCmbAuthToken token, MonCmbDetailComptePayload payload) throws RestClientUnexpectedResponseException
    {
        Assert.notNull(token, "Absence du token");
        Assert.notNull(payload, "Absence du corps de la requête (payload)");

        RestTemplate template = restTemplate(OK);
        HttpEntity<MonCmbDetailComptePayload> request = new HttpEntity<>(payload, defaultHeaders(token));
        ResponseEntity<MonCmbDetailCompte> response = template.exchange(url("/accounts/detailcompte"), POST, request, MonCmbDetailCompte.class);

        return response.getBody();
    }

    public MonCmbSyntheseEpargne syntheseComptesEpargne(MonCmbAuthToken token) throws RestClientUnexpectedResponseException
    {
        Assert.notNull(token, "Absence du token");

        RestTemplate template = restTemplate(OK);
        HttpEntity<String> request = new HttpEntity<>("{}", defaultHeaders(token));
        ResponseEntity<MonCmbSyntheseEpargne> response = template.exchange(url("/accounts/syntheseepargne"), POST, request, MonCmbSyntheseEpargne.class);

        return response.getBody();
    }

    public MonCmbDetailCompteEpargne detailCompteEpargne(MonCmbAuthToken token, MonCmbDetailCompteEpargnePayload payload) throws RestClientUnexpectedResponseException
    {
        Assert.notNull(token, "Absence du token");
        Assert.notNull(payload, "Absence du corps de la requête (payload)");

        RestTemplate template = restTemplate(OK);
        HttpEntity<MonCmbDetailCompteEpargnePayload> request = new HttpEntity<>(payload, defaultHeaders(token));
        ResponseEntity<MonCmbDetailCompteEpargne> response = template.exchange(url("/accounts/detailcompte"), POST, request, MonCmbDetailCompteEpargne.class);

        return response.getBody();
    }


    private HttpHeaders defaultHeaders(MonCmbAuthToken token)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authentication", "Bearer "+token.getIdToken());
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer "+token.getAccessToken());
        headers.add("X-Csrf-Token", token.getAccessToken());
        headers.add("X-ARKEA-EFS", "01");
        headers.add("X-REFERER-TOKEN", "RWDPART");

        return headers;
    }

}
