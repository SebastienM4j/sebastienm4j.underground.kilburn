package xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config;

import org.springframework.util.Assert;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbDetailComptePayload.FiltreOperations;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DataSourceProperties
{
    public static class Credentials
    {
        private String login;
        private String password;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Accounts
    {
        private List<String> include;
        private List<String> exclude;

        public List<String> getInclude() {
            return include;
        }

        public void setInclude(List<String> include) {
            this.include = include;
        }

        public List<String> getExclude() {
            return exclude;
        }

        public void setExclude(List<String> exclude) {
            this.exclude = exclude;
        }
    }


    private Credentials credentials;
    private FiltreOperations period;
    private Accounts accounts;


    public DataSourceProperties(Map<String, String> properties)
    {
        Assert.notEmpty(properties, "Propriétés absentes ou vides");

        String login = properties.get("login");
        String password = properties.get("password");

        Assert.hasText(login, "Identifiant absent ou vide");
        Assert.hasText(password, "Mot de passe absent ou vide");

        this.credentials = new Credentials();
        this.credentials.setLogin(login);
        this.credentials.setPassword(password);

        String period = properties.get("period");
        if(period != null) {
            this.period = FiltreOperations.valueOf(period);
        }

        this.accounts = new Accounts();

        String include = properties.get("include");
        if(include != null) {
            this.accounts.setInclude(Arrays.asList(include.split(";")));
        }

        String exclude = properties.get("exclude");
        if(exclude != null) {
            this.accounts.setExclude(Arrays.asList(exclude.split(";")));
        }
    }


    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public FiltreOperations getPeriod() {
        return period;
    }

    public void setPeriod(FiltreOperations period) {
        this.period = period;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

}
