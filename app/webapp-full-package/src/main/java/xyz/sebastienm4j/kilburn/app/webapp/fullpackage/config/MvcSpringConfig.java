package xyz.sebastienm4j.kilburn.app.webapp.fullpackage.config;

import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

@Configuration
public class MvcSpringConfig
{

    /**
     * Résoud les erreurs 404 en revoyant vers la page d'index afin
     * de pouvoir gérer les URLs HTML5 (pour le router Angular 2).
     *
     * Cf
     * http://stackoverflow.com/questions/31415052/angular-2-0-router-not-working-on-reloading-the-browser/31457808#31457808
     * http://stackoverflow.com/questions/38516667/springboot-angular2-how-to-handle-html5-urls
     *
     * @return Le ErrorViewResolver configuré
     */
    @Bean
    public ErrorViewResolver supportAngular2PathLocationStrategy()
    {
        return (req, st, m) -> {
            return st == HttpStatus.NOT_FOUND
                    ? new ModelAndView("index.html", Collections.<String, Object>emptyMap(), HttpStatus.OK)
                    : null;
        };
    }

}
