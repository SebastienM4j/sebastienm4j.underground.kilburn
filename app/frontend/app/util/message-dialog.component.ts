import { Component }      from '@angular/core';
import { Input }          from '@angular/core';

import { NgbModal }       from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


export enum MessageDialogType
{
    MESSAGE,
    CONFIRM,
    INFO,
    WARNING,
    ERROR,
    SUCCESS
}

export enum MessageDialogColor
{
    MUTED,
    PRIMARY,
    INFO,
    WARNING,
    DANGER,
    SUCCESS
}


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'message-dialog',
    templateUrl: 'message-dialog.component.html',
})
export class MessageDialogComponent
{
    messageDialogType = MessageDialogType;
    
    @Input() type: MessageDialogType;
    @Input() color: MessageDialogColor;
    @Input() message: string;
    
    title: string;
    titleClass = {};
    messageClass = {};
    
    constructor(public modal: NgbActiveModal) {}
    
    ngOnInit()
    {
        switch(this.type)
        {
            case MessageDialogType.MESSAGE:
                this.title = 'Information';
                if(!this.color) {
                    this.color = MessageDialogColor.MUTED;
                }
                break;
                
            case MessageDialogType.CONFIRM:
                this.title = 'Confirmation';
                if(!this.color) {
                    this.color = MessageDialogColor.MUTED;
                }
                break;
                
            case MessageDialogType.INFO:
                this.title = 'Information';
                if(!this.color) {
                    this.color = MessageDialogColor.INFO;
                }
                break;
                
            case MessageDialogType.WARNING:
                this.title = 'Alerte';
                if(!this.color) {
                    this.color = MessageDialogColor.WARNING;
                }
                break;
                
            case MessageDialogType.ERROR:
                this.title = 'Erreur';
                if(!this.color) {
                    this.color = MessageDialogColor.DANGER;
                }
                break;
                
            case MessageDialogType.SUCCESS:
                this.title = 'Information';
                if(!this.color) {
                    this.color = MessageDialogColor.SUCCESS;
                }
                break;
        }
        
        switch(this.color)
        {
            case MessageDialogColor.MUTED:
                this.titleClass['text-muted'] = true;
                this.messageClass['bg-faded'] = true;
                break;
                
            case MessageDialogColor.PRIMARY:
                this.titleClass['text-primary'] = true;
                this.messageClass['bg-primary'] = true;
                this.messageClass['text-white'] = true;
                break;
                
            case MessageDialogColor.INFO:
                this.titleClass['text-info'] = true;
                this.messageClass['bg-info'] = true;
                this.messageClass['text-white'] = true;
                break;
                
            case MessageDialogColor.WARNING:
                this.titleClass['text-warning'] = true;
                this.messageClass['bg-warning'] = true;
                this.messageClass['text-white'] = true;
                break;
                
            case MessageDialogColor.DANGER:
                this.titleClass['text-danger'] = true;
                this.messageClass['bg-danger'] = true;
                this.messageClass['text-white'] = true;
                break;
                
            case MessageDialogColor.SUCCESS:
                this.titleClass['text-success'] = true;
                this.messageClass['bg-success'] = true;
                this.messageClass['text-white'] = true;
                break;
        }
    }
}

