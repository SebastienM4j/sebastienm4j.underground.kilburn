import { NgModule }               from '@angular/core';

import { LongPressDirective }     from './longpress.directive';


@NgModule({
  declarations: [
      LongPressDirective
  ],
  exports: [
      LongPressDirective
  ]
})
export class UtilModule {}
