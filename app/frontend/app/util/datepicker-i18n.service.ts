import { Injectable }        from '@angular/core';
import { Inject }            from '@angular/core';
import { LOCALE_ID }         from '@angular/core';

import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';


const I18N_VALUES: Object = {
    fr: {
        weekdays: ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa', 'Di'],
        months: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc']
    }
};

@Injectable()
export class DatepickerI18n extends NgbDatepickerI18n
{
    constructor(@Inject(LOCALE_ID) private localeId: string)

    {
        super();
    }

    getWeekdayShortName(weekday: number): string
    {
        return I18N_VALUES[this.localeId.substring(0, 2)].weekdays[weekday - 1];
    }
    
    getMonthShortName(month: number): string
    {
        return I18N_VALUES[this.localeId.substring(0, 2)].months[month - 1];
    }
    
    getMonthFullName(month: number): string
    {
        return this.getMonthShortName(month);
    }
    
}
