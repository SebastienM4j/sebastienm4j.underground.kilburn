import { Component }  from '@angular/core';
import { Input }      from '@angular/core';
import { ElementRef } from '@angular/core';


@Component({
    selector: 'app',
    templateUrl: 'app/app.component.html'
})
export class AppComponent
{
    @Input() sidebarWidth: number;

    sidebarAnchored: boolean;
    sidebarOpened: boolean;
    
    constructor(elementRef: ElementRef)
    {
        this.sidebarWidth = elementRef.nativeElement.getAttribute('[sidebarWidth]');
        
        this.sidebarAnchored = false;
        this.sidebarOpened = false;
    }

    handleAnchorChanged(anchor: boolean)
    {
        this.sidebarAnchored = anchor;
        
        let pl = '0px';
        if(anchor) {
            pl = this.sidebarWidth+'px';
        }
        
        let body = document.getElementsByTagName('body')[0];
        body.style.paddingLeft = pl;
        
        let contextMenu = document.getElementsByClassName('angular2-contextmenu');
        if(contextMenu.length > 0) {
            let dropdown = contextMenu[0].getElementsByClassName('dropdown-menu')[0];
            dropdown['style']['marginLeft'] = pl;
        }
    }
    
}
