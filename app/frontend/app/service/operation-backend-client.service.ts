import { Injectable }           from '@angular/core';
import { Http }                 from '@angular/http';
import { Response }             from '@angular/http';

import { Observable }           from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { BackendClientService } from './backend-client.service';


@Injectable()
export class OperationBackendClientService extends BackendClientService
{
    constructor (private http: Http)
    {
        super();
    }
    
    
    private baseUrl(idCompte: number)
    {
        return '/rest/backend/comptes/'+idCompte+'/operations';
    }
    
    
    findAllOrderByDateDesc(idCompte: number): Observable<Object[]>
    {
        return this.http.get(this.baseUrl(idCompte))
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    addOperation(idCompte: number, operation: Object): Observable<void>
    {
        return this.http.post(this.baseUrl(idCompte), operation)
                        .catch(this.handleError);
    }
    
    updateOperation(idCompte: number, operation: Object): Observable<void>
    {
        return this.http.put(this.baseUrl(idCompte)+'/'+operation['id'], operation)
                        .catch(this.handleError);
    }
    
    deleteOperation(idCompte: number, id: number): Observable<void>
    {
        return this.http.delete(this.baseUrl(idCompte)+'/'+id)
                        .catch(this.handleError);
    }
    
    pointageOperation(idCompte: number, id: number, pointage: boolean): Observable<void>
    {
        return this.http.patch(this.baseUrl(idCompte)+'/'+id+'/pointage', { pointee: pointage })
                        .catch(this.handleError);
    }
    
}
