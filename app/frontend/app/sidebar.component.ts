import { Component }     from '@angular/core';
import { Input }         from '@angular/core';
import { Output }        from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { EventEmitter }  from '@angular/core';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: [ 'sidebar.component.css' ]
})
export class SidebarComponent
{
    @Input() width: number;
    @Input('isOpen') opened: boolean;
    @Input('isAnchor') anchor: boolean;
    
    @Output('onOpenChanged') openChangedEvent = new EventEmitter<boolean>();
    @Output('onAnchorChanged') anchorChangedEvent = new EventEmitter<boolean>();
    
    actualWidth: number;
    externalChanges: boolean;
    
    constructor()
    {
        this.externalChanges = false;
    }
    
    ngOnInit()
    {
        this.opening(this.opened);
    }
    
    ngOnChanges(changes: SimpleChanges)
    {
        for(let property in changes)
        {
            if(property === 'opened') {
                this.externalChanges = true;
                this.opening(changes[property].currentValue);
            }
        }
    }
    
    opening(opened: boolean)
    {
        this.actualWidth = this.width;
        if(!opened) {
            this.actualWidth = 0;
        }
    }
    
    toggleOpen()
    {
        this.opened = !this.opened;
        this.opening(this.opened);
        this.openChangedEvent.emit(this.opened);
        
        if(!this.opened && this.anchor) {
            this.toggleAnchor();
        }
    }
    
    toggleAnchor()
    {
        this.anchor = !this.anchor;
        this.anchorChangedEvent.emit(this.anchor);
        
        if(!this.anchor && this.opened) {
            this.toggleOpen();
        }
    }
    
    close()
    {
        if(!this.externalChanges && this.opened && !this.anchor) {
            this.toggleOpen();
        }
        this.externalChanges = false;
    }
}
