import { Component }                          from '@angular/core';
import { Router }                             from '@angular/router';
import { ActivatedRoute }                     from '@angular/router';

import { NgbModal }                           from '@ng-bootstrap/ng-bootstrap';

import * as _                                 from 'underscore';

import { OperationRegleBackendClientService } from '../../service/operationregle-backend-client.service';

import { MessageDialogComponent }             from '../../util/message-dialog.component';
import { MessageDialogType }                  from '../../util/message-dialog.component';
import { MessageDialogColor }                 from '../../util/message-dialog.component';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'regle-list',
    templateUrl: 'regle-list.component.html',
    providers: [ OperationRegleBackendClientService ]
})
export class RegleListComponent
{
    types: string[];
    selectedType: string;

    regles: Object[];
    displayedRegles: Object[];
    
    textFilter: string;

    form: boolean;
    formMode: string;
    editedRegle: Object;

    reorder: boolean;
    reorderedRegle: Object;
    
    constructor(private router: Router,
                private route: ActivatedRoute,
                private modal: NgbModal,
                private operationRegleBackendClientService: OperationRegleBackendClientService)
    {
        this.form = false;
        this.reorder = false;
        this.selectedType = this.route.snapshot.queryParams['type'];
    }
    
    ngOnInit()
    {
        this.operationRegleBackendClientService.getOperationRegleTypes().subscribe(types =>
        {
            this.types = types;
            this.selectType(this.selectedType);
        });
    }
    
    populateDisplayedRegles()
    {
        this.displayedRegles = this.regles;
        this.orderDisplayedRegles();
    }
    
    orderDisplayedRegles()
    {
        this.displayedRegles.sort(function(l: Object, r: Object)
        {
            if(l['ordre'] < r['ordre']) {
                return -1;
            } else if(l['ordre'] > r['ordre']) {
                return 1;
            }
            
            return 0;
        });
    }
    
    /*
     * Type de règle
     */
    
    selectType(t: string)
    {
        if(!t) {
            return;
        }
        
        this.selectedType = t;
        this.operationRegleBackendClientService.findAllByType(t).subscribe(regles =>
        {
            this.regles = regles;
            this.populateDisplayedRegles();
        });
        
        this.form = false;
    }
    
    /*
     * Filtre
     */

    filterByText(textFilter: string)
    {
        this.textFilter = textFilter;
        this.filter();
    }

    filter()
    {
        this.populateDisplayedRegles();

        if(this.textFilter)
        {
            let search = this.textFilter.toLowerCase();
            
            this.displayedRegles = _.filter(this.displayedRegles, (r) =>
            {
                if(r['libelle'].toLowerCase().indexOf(search) !== -1) {
                    return true;
                }
                return false;
            });
        }
    }
    
    /*
     * Liste des règles
     */
    
    newRegle()
    {
        this.form = true;
        this.formMode = 'new';
        this.editedRegle = {
            regle: this.selectedType,
            langage: 'GROOVY' 
        };
    }
    
    editRegle(regle: Object)
    {
        if(this.reorder) {
            return;
        }
        
        this.operationRegleBackendClientService.getOperationRegle(regle['id']).subscribe(r =>
        {
            this.form = true;
            this.formMode = 'update';
            this.editedRegle = r;
        });
    }
    
    deleteRegle(regle: Object)
    {
        let confirmDialog = this.modal.open(MessageDialogComponent);
        confirmDialog.componentInstance.type = MessageDialogType.CONFIRM;
        confirmDialog.componentInstance.color = MessageDialogColor.WARNING;
        confirmDialog.componentInstance.message = 'Confirmez-vous la suppression de la règle '+regle['libelle']+' ?';
        
        confirmDialog.result.then(() =>
        {
            this.operationRegleBackendClientService.deleteOperationRegle(regle['id']).subscribe(() =>
            {
                this.regles = _.reject(this.regles, (c) => c['id'] === regle['id']);
                this.filter();
                
                if(this.editedRegle && this.editedRegle['id'] === regle['id']) {
                    this.form = false;
                }
            });
        }, () => {});
    }
    
    reorderRegle(regle: Object)
    {
        this.reorderedRegle = regle;
        this.reorder = true;
    }
    
    reorderUp()
    {
        if(this.reorderedRegle['ordre'] > 1)
        {
            let aboveRegle = _.find(this.regles, r => r['ordre'] === this.reorderedRegle['ordre']-1);
            aboveRegle['ordre'] += 1;
            this.reorderedRegle['ordre'] -= 1;
            
            this.orderDisplayedRegles();
        }
    }
    
    reorderDown()
    {
        if(this.reorderedRegle['ordre'] < this.regles.length)
        {
            let underRegle = _.find(this.regles, r => r['ordre'] === this.reorderedRegle['ordre']+1);
            underRegle['ordre'] -= 1;
            this.reorderedRegle['ordre'] += 1;
            
            this.orderDisplayedRegles();
        }
    }
    
    reorderValidate()
    {
        this.operationRegleBackendClientService.reorderOperationRegle(this.reorderedRegle['id'], this.reorderedRegle['ordre']).subscribe(() =>
        {
            this.reorder = false;
        });
    }
    
    /*
     * Formulaire
     */
    
    regleSaved(regle: Object)
    {
        this.formMode = 'update';
        this.editedRegle = regle;
        
        this.operationRegleBackendClientService.findAllByType(this.selectedType).subscribe(regles =>
        {
            this.regles = regles;
            this.populateDisplayedRegles();
        });
    }
    
    closeForm()
    {
        this.form = false;
        this.editedRegle = null;
    }
    
}
