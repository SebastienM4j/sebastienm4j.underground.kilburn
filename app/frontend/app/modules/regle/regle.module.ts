import { NgModule }               from '@angular/core';
import { CommonModule }           from '@angular/common';
import { FormsModule }            from '@angular/forms';

import { NgbModule }              from '@ng-bootstrap/ng-bootstrap';
import { AceEditorComponent }     from 'ng2-ace-editor';

import { ContextMenuModule }      from 'angular2-contextmenu';

import { UtilModule }             from '../../util/util.module';

import { RegleRoutingModule }     from './regle.routing-module';

import { RegleListComponent }     from './regle-list.component';
import { RegleFormComponent }     from './regle-form.component';

import { MessageDialogComponent } from '../../util/message-dialog.component';


@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      NgbModule,
      ContextMenuModule,
      UtilModule,
      RegleRoutingModule
  ],
  declarations: [
      RegleListComponent,
      RegleFormComponent,
      AceEditorComponent
  ],
  entryComponents: [
      MessageDialogComponent
  ]
})
export class RegleModule {}
