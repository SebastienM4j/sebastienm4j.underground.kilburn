import { Component }     from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { Input }         from '@angular/core';
import { Output }        from '@angular/core';
import { EventEmitter }  from '@angular/core';

import * as _            from 'underscore';



@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'operation-list-filters',
    templateUrl: 'operation-list-filters.component.html'
})
export class OperationListFiltersComponent
{
    @Input() filters: Filters;
    @Input() operations: Object[];
    
    @Output('onFilter') filterEvent = new EventEmitter();

    count: number;
    
    constructor() {}
    
    ngOnInit()
    {
        if(this.operations)
        {
            this.count = this.operations.length;
        
            this.populateModeFilter(this.operations);
            this.populateTagFilter(this.operations);
        }
    }
    
    ngOnChanges(changes: SimpleChanges)
    {
        for(let propName in changes)
        {
            if(propName === 'operations')
            {
                if(this.operations)
                {
                    this.count = this.operations.length;
                
                    this.populateModeFilter(this.operations);
                    this.populateTagFilter(this.operations);
                }
                break;
            }
        }
    }
    
    
    /*
     * Populate filter
     */
    
    populateModeFilter(operations: Object[])
    {
        let selected: string[] = [];
        if(this.filters.modeFilter != null) {
            selected = Object.assign([], this.filters.modeFilter).filter(f => f['filter']).map(f => f['label']);
        }
        
        this.filters.modeFilter = _.map(_.uniq(_.pluck(operations, 'modeLibelle')), (f) =>
        {
            return { 'label': f, 'filter': (selected && selected.indexOf(f) !== -1) };
        });
        this.filters.modeFilter.sort((l, r) => this.sortTypeOrTagFilter(l, r));
    }
    
    populateTagFilter(operations: Object[])
    {
        let selected: string[] = [];
        if(this.filters.tagFilter != null) {
            selected = Object.assign([], this.filters.tagFilter).filter(f => f['filter']).map(f => f['label']);
        }
        
        this.filters.tagFilter = _.map(_.uniq(_.flatten(_.pluck(operations, 'tags'))), (f) =>
        {
            return { 'label': f, 'filter': (selected && selected.indexOf(f) !== -1) };
        });
        this.filters.tagFilter.sort((l, r) => this.sortTypeOrTagFilter(l, r));
    }
    
    sortTypeOrTagFilter(l: Object, r: Object)
    {
        if(l['label'] < r['label']) {
            return -1;
        } else if(l['label'] > r['label']) {
            return 1;
        }
        return 0;
    }
    
    
    /*
     * Filtrage
     */
    
    filterByModeOrTag(filter: Object)
    {
        filter['filter'] = !filter['filter'];
        this.filter();
    }
    
    filterByPointage()
    {
        setTimeout(() => this.filter(), 0);
    }
    
    filter()
    {
        this.filterEvent.emit(this.filters);
    }
    
}


export class Filters
{
    public dateDebFilter: Object;
    public dateFinFilter: Object;
    public dateValeurDebFilter: Object;
    public dateValeurFinFilter: Object;
    public modeFilter: Object[];
    public pointageFilter: any = 'all';
    public tagFilter: Object[];
}

