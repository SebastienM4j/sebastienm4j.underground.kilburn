import { Component }    from '@angular/core';
import { Input }        from '@angular/core';
import { Output }       from '@angular/core';
import { EventEmitter } from '@angular/core';

import { Observable }   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import * as _           from 'underscore';
import * as moment      from 'moment';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'operation-list-form',
    templateUrl: 'operation-list-form.component.html'
})
export class OperationListFormComponent
{
    @Input() mode: string;
    @Input() operation: Object;
    @Input() operations: Object[];
    
    @Output('onSave') onSaveEvent = new EventEmitter<Object>();
    @Output('onSaveAndContinue') onSaveAndContinueEvent = new EventEmitter<Object>();
    @Output('onCancel') onCancelEvent = new EventEmitter();

    editedOperation: Object;
    editedOperationNewTag: string;
    
    existingTiers: string[];
    searchTiers: any;
    
    existingTag: string[];
    searchTag: any;

    constructor() {}
    
    ngOnInit()
    {
        this.editedOperation = this.mode === 'update' ? _.clone(this.operation) : {};
        this.editedOperation['dateForPicker'] = this.dateToPicker(this.editedOperation['date']);
        this.editedOperation['dateValeurForPicker'] = this.dateToPicker(this.editedOperation['dateValeur']);
        
        this.populateTypeaheads();
    }
    
    populateTypeaheads()
    {
        this.existingTiers = _.uniq(_.pluck(this.operations, 'tiers'));
        this.searchTiers = (text$: Observable<string>) =>
            text$.debounceTime(200)
                 .distinctUntilChanged()
                 .map(term => term === ''
                        ? []
                        : this.existingTiers.filter(v => new RegExp(term, 'gi').test(v)).slice(0, 10));
        
        this.existingTag = _.uniq(_.flatten(_.pluck(this.operations, 'tags')));
        this.searchTag = (text$: Observable<string>) =>
            text$.debounceTime(200)
                 .distinctUntilChanged()
                 .map(term => term === ''
                        ? []
                        : this.existingTag.filter(v => new RegExp(term, 'gi').test(v)).slice(0, 10));
    }
    
    /*
     * Tags
     */
    
    editedOperationAddTag()
    {
        if(!this.editedOperation['tags']) {
            this.editedOperation['tags'] = [];
        }
        this.editedOperation['tags'].push(this.editedOperationNewTag);
        this.editedOperationNewTag = null;
    }
    
    editedOperationDeleteTag(t: string)
    {
        this.editedOperation['tags'] = _.without(this.editedOperation['tags'], t);
    }
    
    /*
     * Actions
     */
    
    editedOperationSave()
    {
        this.editedOperation['date'] = this.pickerToDate(this.editedOperation['dateForPicker']);
        this.editedOperation['dateValeur'] = this.pickerToDate(this.editedOperation['dateValeurForPicker']);
        
        this.onSaveEvent.emit(this.editedOperation);
    }
    
    editedOperationSaveAndContinue()
    {
        this.editedOperation['date'] = this.pickerToDate(this.editedOperation['dateForPicker']);
        this.editedOperation['dateValeur'] = this.pickerToDate(this.editedOperation['dateValeurForPicker']);
        
        this.onSaveAndContinueEvent.emit(this.editedOperation);
        
        this.operations.push(this.editedOperation);
        this.populateTypeaheads();
        
        this.editedOperation = {};
    }
    
    editedOperationCancel()
    {
        this.onCancelEvent.emit();
    }
    
    /*
     * 
     */
    
    dateToPicker(d: any)
    {
        if(!d) {
            return null;
        }
        
        let date = moment(d);
        return {
            day: date.date(),
            month: date.month()+1,
            year: date.year()
        };
    }
    
    pickerToDate(d: Object)
    {
        if(!d) {
            return null;
        }
        
        return moment().year(d['year']).month(d['month']-1).date(d['day'])
                       .hour(0).minute(0).second(0).millisecond(0)
                       .toISOString();
    }
    
}