import { NgModule }                       from '@angular/core';
import { CommonModule }                   from '@angular/common';
import { FormsModule }                    from '@angular/forms';

import { NgbModule }                      from '@ng-bootstrap/ng-bootstrap';

import { ContextMenuModule }              from 'angular2-contextmenu';

import { OperationRoutingModule }         from './operation.routing-module';
import { OperationListComponent }         from './operation-list.component';
import { OperationListFiltersComponent }  from './operation-list-filters.component';
import { OperationListFormComponent }     from './operation-list-form.component';
import { OperationListRuleFormComponent } from './operation-list-ruleform.component';

import { MessageDialogComponent }         from '../../util/message-dialog.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        ContextMenuModule,
        OperationRoutingModule
    ],
    declarations: [
        OperationListComponent,
        OperationListFiltersComponent,
        OperationListFormComponent,
        OperationListRuleFormComponent
    ],
   entryComponents: [
       MessageDialogComponent
   ]
})
export class OperationModule {}

