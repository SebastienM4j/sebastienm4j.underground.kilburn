import { Component }                          from '@angular/core';
import { ViewChild }                          from '@angular/core';
import { ElementRef }                         from '@angular/core';
import { HostListener }                       from '@angular/core';
import { ActivatedRoute }                     from '@angular/router';
import { CurrencyPipe }                       from '@angular/common';
import { DatePipe }                           from '@angular/common';

import { NgbDatepickerI18n }                  from '@ng-bootstrap/ng-bootstrap';
import { NgbDateParserFormatter }             from '@ng-bootstrap/ng-bootstrap';
import { NgbModal }                           from '@ng-bootstrap/ng-bootstrap';

import * as _                                 from 'underscore';
import * as Decimal                           from 'decimal.js';
import * as moment                            from 'moment';

import { Filters }                            from './operation-list-filters.component';

import { CompteBackendClientService }         from '../../service/compte-backend-client.service';
import { OperationBackendClientService }      from '../../service/operation-backend-client.service';
import { OperationRegleBackendClientService } from '../../service/operationregle-backend-client.service';

import { DatepickerI18n }                     from '../../util/datepicker-i18n.service';
import { DatepickerMomentMapper }             from '../../util/datepicker-moment-mapper.service';

import { MessageDialogComponent }             from '../../util/message-dialog.component';
import { MessageDialogType }                  from '../../util/message-dialog.component';
import { MessageDialogColor }                 from '../../util/message-dialog.component';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'operation-list',
    templateUrl: 'operation-list.component.html',
    providers: [
        {
            provide: NgbDatepickerI18n,
            useClass: DatepickerI18n
        }, {
            provide: NgbDateParserFormatter,
            useFactory: () => { return new DatepickerMomentMapper("DD/MM/YYYY") }
        },
        CurrencyPipe,
        DatePipe,
        CompteBackendClientService,
        OperationBackendClientService,
        OperationRegleBackendClientService
    ]
})
export class OperationListComponent
{
    @ViewChild('colSelection') colSelection: ElementRef;
    @ViewChild('colDate') colDate: ElementRef;
    @ViewChild('colMode') colMode: ElementRef;
    @ViewChild('colTiers') colTiers: ElementRef;
    @ViewChild('colTags') colTags: ElementRef;
    @ViewChild('colMontant') colMontant: ElementRef;
    @ViewChild('colPointage') colPointage: ElementRef;
    
    colSelectionWidth: number;
    colDateWidth: number;
    colModeWidth: number;
    colTiersWidth: number;
    colTagsWidth: number;
    colMontantWidth: number;
    colPointageWidth: number;
    tblOperationHeight: number;

    idCompte: number;
    
    soldes: Object;
    operations: Object[];
    displayedOperations: Object[];

    filters: boolean;
    textFilter: string;
    propFilters: Filters;
    
    colOrder: string;
    
    form: boolean;
    formMode: string;
    editedOperation: Object;
    
    selection: boolean;
    selectionAll: boolean;
    
    formRule: boolean;

    constructor(private route: ActivatedRoute,
                private modal: NgbModal,
                private currencyPipe: CurrencyPipe,
                private datePipe: DatePipe,
                private compteBackendClientService: CompteBackendClientService,
                private operationBackendClientService: OperationBackendClientService,
                private operationRegleBackendClientService: OperationRegleBackendClientService)
    {
        this.idCompte = this.route.snapshot.params['id'];
        this.filters = false;   
        this.propFilters = new Filters();
        this.colOrder = '-date';
    }
    
    ngOnInit()
    {
        let solde = this.route.snapshot.data['solde'];
        this.soldes = {
            pointe: new Decimal(solde['pointe']),
            avenir: new Decimal(solde['avenir']),
            solde: new Decimal(solde['solde'])
        };
        
        this.operations = this.route.snapshot.data['operations'];
        this.populateDisplayedOperations();
    }
    
    ngAfterViewInit()
    {
        this.resizeHeaderColumns();
        this.resizeBodyHeight();
    }
    
    populateDisplayedOperations()
    {
        let selected = this.getSelectedIds();
    
        this.displayedOperations = _.map(this.operations, (o) =>
        {
            let op = Object.assign({}, o);
            switch(op['mode']) {
                case 'CARTE':
                    op['modeLibelle'] = 'Carte';
                    break;
                case 'RETRAIT':
                    op['modeLibelle'] = 'Retrait';
                    break;
                case 'VERSEMENT':
                    op['modeLibelle'] = 'Versement';
                    break;
                case 'CHEQUE':
                    op['modeLibelle'] = 'Chèque';
                    break;
                case 'REMISE':
                    op['modeLibelle'] = 'Remise';
                    break;
                case 'VIREMENT':
                    op['modeLibelle'] = 'Virement';
                    break;
                case 'PRELEVEMENT':
                    op['modeLibelle'] = 'Prélèvement';
                    break;
            }
            
            if(op['tags']) {
                op['tags'].sort((l: string, r: string) => l < r ? -1 : (l > r ? 1 : 0));
            }
            
            op['selected'] = _.contains(selected, op['id']);
            
            return op;
        });
    }
    
    /*
     * Gestion de l'affichage du tableau
     */
    
    resizeHeaderColumns()
    {
        if(this.selection) {
            this.colSelectionWidth = this.colSelection.nativeElement.offsetWidth;
        }
        this.colDateWidth = this.colDate.nativeElement.offsetWidth;
        this.colModeWidth = this.colMode.nativeElement.offsetWidth;
        this.colTiersWidth = this.colTiers.nativeElement.offsetWidth;
        this.colTagsWidth = this.colTags.nativeElement.offsetWidth;
        this.colMontantWidth = this.colMontant.nativeElement.offsetWidth;
        this.colPointageWidth = this.colPointage.nativeElement.offsetWidth;
    }
    
    resizeBodyHeight()
    {
        this.tblOperationHeight = window.innerHeight-180;
    }
    
    @HostListener('window:resize')
    onWindowResize() {
        this.resizeBodyHeight();
    }
    
    /*
     * Filtre
     */

    filterByText(textFilter: string)
    {
        this.textFilter = textFilter;
        this.filter();
    }
    
    filterByProperty(filters: Filters)
    {
        this.propFilters = filters;
        this.filter();
    }
    
    filter()
    {
        this.populateDisplayedOperations();

        // Champ de recherche rapide
        
        if(this.textFilter)
        {
            let search = this.textFilter.toLowerCase();
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                let dateTxt = this.datePipe.transform(o['date'], 'shortDate');
                let dateValeurTxt = this.datePipe.transform(o['dateValeur'], 'shortDate');
                
                if((dateTxt && dateTxt.indexOf(search) !== -1) ||
                   (dateValeurTxt && dateValeurTxt.indexOf(search) !== -1) ||
                   (o['modeLibelle'] &&  o['modeLibelle'].toLowerCase().indexOf(search) !== -1) ||
                   (o['tiers']&& o['tiers'].toLowerCase().indexOf(search) !== -1) ||
                   (o['reference'] && o['reference'].toLowerCase().indexOf(search) !== -1) ||
                   (o['libelle'] && o['libelle'].toLowerCase().indexOf(search) !== -1) ||
                   (o['commentaire'] && o['commentaire'].toLowerCase().indexOf(search) !== -1) ||
                   (o['montant'] && (o['montant']+'').toLowerCase().indexOf(search) !== -1)) {
                    return true;
                }
                
                if(o['tags'] && o['tags'].length > 0 &&
                    _.some(o['tags'], (t: string) => t.toLowerCase().indexOf(search) !== -1)) {
                     return true;
                 }
                
                return false;
            });
        }
        
        // Filtre par date
        
        if(this.propFilters.dateDebFilter)
        {
            let date = this.getSelectedDate(this.propFilters.dateDebFilter);
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                return o['date'] && moment(o['date']).isSameOrAfter(date);
            });
        }
        
        if(this.propFilters.dateFinFilter)
        {
            let date = this.getSelectedDate(this.propFilters.dateFinFilter);
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                return o['date'] && moment(o['date']).isSameOrBefore(date);
            });
        }
        
        // Filtre par date de valeur
        
        if(this.propFilters.dateValeurDebFilter)
        {
            let date = this.getSelectedDate(this.propFilters.dateValeurDebFilter);
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                return o['dateValeur'] && moment(o['dateValeur']).isSameOrAfter(date);
            });
        }
        
        if(this.propFilters.dateValeurFinFilter)
        {
            let date = this.getSelectedDate(this.propFilters.dateValeurFinFilter);
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                return o['dateValeur'] && moment(o['dateValeur']).isSameOrBefore(date);
            });
        }
        
        // Filtre par mode
        
        if(this.propFilters.modeFilter)
        {
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                for(let f of this.propFilters.modeFilter) {
                    if(f['filter'] && o['modeLibelle'] !== f['label']) {
                        return false;
                    }
                }
                return true;
            });
        }
        
        // Filtre par tag
        
        if(this.propFilters.tagFilter)
        {
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                for(let f of this.propFilters.tagFilter) {
                    if(f['filter'] && !this.hasTag(o, f['label'])) {
                        return false;
                    }
                }
                return true;
            });
        }
        
        // Filtre pointage
        
        if(this.propFilters.pointageFilter !== 'all')
        {
            this.displayedOperations = _.filter(this.displayedOperations, (o) =>
            {
                return (this.propFilters.pointageFilter === 'true' && o['pointee']) ||
                       (this.propFilters.pointageFilter === 'false' && !o['pointee']);
            });
        }
        
        // On redimentionne les colonnes de l'entête du tableau
        // car leur taille peut varier en fonction des données
        // affichées, donc du filtrage
        setTimeout(() => this.resizeHeaderColumns(), 0);
    }
    
    getSelectedDate(date: Object)
    {
        let ds = _.clone(date);
        ds['month']--;
        return moment(ds);
    }
    
    hasTag(op: Object, tag: string)
    {
        if(!op['tags'] || op['tags'].length === 0) {
            return false;
        }
        return _.some(op['tags'], (t) => t === tag);
    }
    
    /*
     * Tri
     */
    
    orderBy(col: string)
    {
        switch(col)
        {
            case 'date':
                switch(this.colOrder) {
                    case 'date':        this.colOrder = '-date';       break;
                    case '-date':       this.colOrder = 'dateValeur';  break;
                    case 'dateValeur':  this.colOrder = '-dateValeur'; break;
                    case '-dateValeur': this.colOrder = 'date';        break;
                    default:            this.colOrder = 'date';        break;
                }
                break;
                
            case 'mode':
                switch(this.colOrder) {
                    case 'mode':    this.colOrder = '-mode'; break;
                    case '-mode':   this.colOrder = 'mode';  break;
                    default:        this.colOrder = 'mode';  break;
                }
                break;
                
            case 'tiers':
                switch(this.colOrder) {
                    case 'tiers':    this.colOrder = '-tiers';   break;
                    case '-tiers':   this.colOrder = 'libelle';  break;
                    case 'libelle':  this.colOrder = '-libelle'; break;
                    case '-libelle': this.colOrder = 'tiers';    break;
                    default:         this.colOrder = 'tiers';    break;
                }
                break;
                
            case 'montant':
                switch(this.colOrder) {
                    case 'montant':  this.colOrder = '-montant'; break;
                    case '-montant': this.colOrder = 'montant';  break;
                    default:         this.colOrder = 'montant';  break;
                }
                break;
        }
        
        this.order();
    }
    
    order()
    {
        let property: string = null;
        let order: number = 1;
        switch(this.colOrder)
        {
            case 'date':
            case 'dateValeur':
            case 'tiers':
            case 'libelle':
            case 'montant':
                property = this.colOrder;
                order = 1;
                break;
            
            case 'mode':
                property = 'modeLibelle';
                order = 1;
                break;
                
            case '-mode':
                property = 'modeLibelle';
                order = -1;
                break;
                
            case '-date':
            case '-dateValeur':
            case '-tiers':
            case '-libelle':
            case '-montant':
                property = this.colOrder.replace('-', '');
                order = -1;
                break;
        }
        
        this.displayedOperations.sort(function(l: Object, r: Object)
        {
            let comp = 0;
            if(l[property] < r[property]) {
                comp = -1;
            } else if(l[property] > r[property]) {
                comp = 1;
            }
            
            return comp*order;
        });
    }
    
    /*
     * Actions menus contextuel
     */
    
    updateOperation(op: Object)
    {
        this.form = true;
        this.formMode = 'update';
        this.editedOperation = _.clone(op);
    }
    
    deleteOperation(op: Object)
    {
        let confirmDialog = this.modal.open(MessageDialogComponent, { size: 'lg' });
        confirmDialog.componentInstance.type = MessageDialogType.CONFIRM;
        confirmDialog.componentInstance.color = MessageDialogColor.WARNING;
        confirmDialog.componentInstance.message = 'Confirmez-vous la suppression cette l\'opération d\'un montant de '+this.currencyPipe.transform(op['montant'], 'EUR', true)+' ?';
        
        confirmDialog.result.then(() =>
        {
            this.operationBackendClientService.deleteOperation(this.idCompte, op['id'])
                                              .subscribe(() =>
            {
                this.operations = _.reject(this.operations, (o) => o['id'] === op['id']);
                this.filter();
                
                this.recalculateSoldes(op, null);
            });
        }, () => {});
    }
    
    selectAll()
    {
        _.each(this.displayedOperations, (op) => op['selected'] = this.selectionAll);
    }
    
    selectionFor(action: string, op: Object)
    {
        this.selection = true;
        op['selected'] = true;
        
        switch(action)
        {
            case 'RULE':
                this.formRule = true;
                break;
        }
        
        setTimeout(() => this.resizeHeaderColumns(), 0);
    }
    
    /*
     * Ajout / modification
     */
    
    newOperation()
    {
        this.form = true;
        this.formMode = 'new';
    }
    
    handleSave(op: Object)
    {
        if(this.formMode === 'new')
        {
            this.operationBackendClientService.addOperation(this.idCompte, op)
                                              .subscribe(() =>
            {
                this.saveOperation(op);
                this.form = false;
                this.editedOperation = null;
            });
        }
        else
        {
            this.operationBackendClientService.updateOperation(this.idCompte, op)
                                              .subscribe(() =>
            {
                this.saveOperation(op);
                this.form = false;
                this.editedOperation = null;
            });
        }
    }
    
    handleSaveContinue(op: Object)
    {
        if(this.formMode === 'new')
        {
            this.operationBackendClientService.addOperation(this.idCompte, op)
                                              .subscribe(() =>
            {
                this.saveOperation(op);
            });
        }
        else
        {
            this.operationBackendClientService.updateOperation(this.idCompte, op)
                                              .subscribe(() =>
            {
                this.saveOperation(op);
            });
        }
    }
    
    saveOperation(op: Object)
    {
        this.recalculateSoldes(this.editedOperation, op);
        
        this.operations = _.reject(this.operations, (o) => o['id'] === op['id']);
        this.operations.push(op);
        this.filter();
        this.order();
    }
    
    handleCancelForm()
    {
        this.form = false;
        this.editedOperation = null;
    }
    
    pointageOperation(op: Object)
    {   
        let previousOp = _.clone(op);
        previousOp['pointee'] = !previousOp['pointee'];
        
        this.operationBackendClientService.pointageOperation(this.idCompte, op['id'], op['pointee'])
                                          .subscribe(() =>
        {
            this.recalculateSoldes(previousOp, op);
            
            if(this.propFilters.pointageFilter !== 'all')
            {
                let originalOp = _.find(this.operations, (o) => o['id'] === op['id']);
                originalOp['pointee'] = op['pointee'];
                
                this.filter();
            }
        });
    }
    
    /*
     * Application d'une règle
     */
    
    applyRule(regle: Object)
    {
        if(this.checkSelection())
        {
            this.operationRegleBackendClientService.applyOperationRegle(regle['id'], this.getSelectedIds())
                                                   .subscribe(() =>
            {
                this.operationBackendClientService.findAllOrderByDateDesc(this.idCompte)
                                                  .subscribe(operations => 
                {
                    this.operations = operations;
                    this.filter();
                });
            });
        }
    }
    
    cancelRule()
    {
        this.formRule = false;
        this.selection = false;
    }
    
    /*
     * Commun
     */
    
    recalculateSoldes(previousOperation: Object, currentOperation: Object)
    {
        if(previousOperation)
        {
            if(previousOperation['pointee']) {
                this.soldes['pointe'] = this.soldes['pointe'].minus(previousOperation['montant']);
            } else {
                this.soldes['avenir'] = this.soldes['avenir'].minus(previousOperation['montant']);
            }
        }
        
        if(currentOperation)
        {
            if(currentOperation['pointee']) {
                this.soldes['pointe'] = this.soldes['pointe'].plus(currentOperation['montant']);
            } else {
                this.soldes['avenir'] = this.soldes['avenir'].plus(currentOperation['montant']);
            }
        }
        
        this.soldes['solde'] = this.soldes['pointe'].plus(this.soldes['avenir']);
    }
    
    checkSelection(): boolean
    {
        let hasSelection = _.some(this.displayedOperations, (op) => op['selected']);
        
        if(!hasSelection) {
            let dialog = this.modal.open(MessageDialogComponent, { size: 'lg' });
            dialog.componentInstance.type = MessageDialogType.WARNING;
            dialog.componentInstance.message = 'Aucune opération n\'est sélectionnée, l\'action ne peut pas être réalisée.';
        }
        
        return hasSelection;
    }
    
    getSelectedIds(): number[]
    {
        return _.pluck(_.filter(this.displayedOperations, (op) => op['selected']), 'id');
    }
    
}
