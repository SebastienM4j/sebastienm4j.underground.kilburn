package xyz.sebastienm4j.kilburn.core.operation.importation.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@ConfigurationProperties(prefix="operation-importation")
public class CoreOperationImportationProperties
{
    public static class Scheduling
    {
        private String cron;
        private Long period;
        private TimeUnit timeUnit = TimeUnit.MINUTES;
        private long initialDelay = 0;
        private boolean fixedRate = false;

        public String getCron() {
            return cron;
        }

        public void setCron(String cron) {
            this.cron = cron;
        }

        public Long getPeriod() {
            return period;
        }

        public void setPeriod(Long period) {
            this.period = period;
        }

        public TimeUnit getTimeUnit() {
            return timeUnit;
        }

        public void setTimeUnit(TimeUnit timeUnit) {
            this.timeUnit = timeUnit;
        }

        public long getInitialDelay() {
            return initialDelay;
        }

        public void setInitialDelay(long initialDelay) {
            this.initialDelay = initialDelay;
        }

        public boolean isFixedRate() {
            return fixedRate;
        }

        public void setFixedRate(boolean fixedRate) {
            this.fixedRate = fixedRate;
        }
    }

    public static class Import
    {
        private Scheduling scheduling;
        private String datasourceClassName;
        private Map<String, String> datasourceProperties;

        public Scheduling getScheduling() {
            return scheduling;
        }

        public void setScheduling(Scheduling scheduling) {
            this.scheduling = scheduling;
        }

        public String getDatasourceClassName() {
            return datasourceClassName;
        }

        public void setDatasourceClassName(String datasourceClassName) {
            this.datasourceClassName = datasourceClassName;
        }

        public Map<String, String> getDatasourceProperties() {
            return datasourceProperties;
        }

        public void setDatasourceProperties(Map<String, String> datasourceProperties) {
            this.datasourceProperties = datasourceProperties;
        }
    }


    private List<Import> imports;


    public List<Import> getImports() {
        return imports;
    }

    public void setImports(List<Import> imports) {
        this.imports = imports;
    }

}
