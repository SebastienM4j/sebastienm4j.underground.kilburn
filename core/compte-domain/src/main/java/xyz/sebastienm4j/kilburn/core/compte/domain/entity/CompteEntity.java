package xyz.sebastienm4j.kilburn.core.compte.domain.entity;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;

import javax.persistence.*;
import java.util.Set;

/**
 * Représente un compte bancaire.
 */
@Entity
@Table(name="compte")
public class CompteEntity implements Compte
{
    private Integer id;

    private Type type;
    private String libelle;

    private String numero;
    private String iban;

    private String ribBanque;
    private String ribGuichet;
    private String ribCompte;
    private String ribCle;

    private String bic;

    private Set<String> tags;



    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    @Column(name="type", length=10)
    @Enumerated(EnumType.STRING)
    public Type getType()
    {
        return type;
    }

    @Override
    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    @Column(name="libelle", length=100, nullable=false)
    public String getLibelle()
    {
        return libelle;
    }

    @Override
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    @Override
    @Column(name="numero", nullable=false)
    public String getNumero()
    {
        return numero;
    }

    @Override
    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    @Override
    @Column(name="iban", length=27)
    public String getIban()
    {
        return iban;
    }

    @Override
    public void setIban(String iban)
    {
        this.iban = iban;
    }

    @Override
    @Column(name="rib_banque", length=5)
    public String getRibBanque()
    {
        return ribBanque;
    }

    @Override
    public void setRibBanque(String ribBanque)
    {
        this.ribBanque = ribBanque;
    }

    @Override
    @Column(name="rib_guichet", length=5)
    public String getRibGuichet()
    {
        return ribGuichet;
    }

    @Override
    public void setRibGuichet(String ribGuichet)
    {
        this.ribGuichet = ribGuichet;
    }

    @Override
    @Column(name="rib_compte", length=11)
    public String getRibCompte()
    {
        return ribCompte;
    }

    @Override
    public void setRibCompte(String ribCompte)
    {
        this.ribCompte = ribCompte;
    }

    @Override
    @Column(name="rib_cle", length=2)
    public String getRibCle()
    {
        return ribCle;
    }

    @Override
    public void setRibCle(String ribCle)
    {
        this.ribCle = ribCle;
    }

    @Override
    @Column(name="bic", length=11)
    public String getBic()
    {
        return bic;
    }

    @Override
    public void setBic(String bic)
    {
        this.bic = bic;
    }

    @Override
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(
        name="compte_tag",
        joinColumns=@JoinColumn(name="compte", referencedColumnName="id")
    )
    @Column(name="tag", length=50, nullable=false)
    public Set<String> getTags()
    {
        return tags;
    }

    @Override
    public void setTags(Set<String> tags)
    {
        this.tags = tags;
    }

}
