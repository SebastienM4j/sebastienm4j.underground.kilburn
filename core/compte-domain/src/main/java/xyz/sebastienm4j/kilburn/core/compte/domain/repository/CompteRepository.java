package xyz.sebastienm4j.kilburn.core.compte.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;
import xyz.sebastienm4j.kilburn.core.compte.domain.queryresult.CompteSoldeQueryResult;
import xyz.sebastienm4j.kilburn.core.compte.domain.queryresult.SoldeQueryResult;

import java.util.List;

@Repository
public interface CompteRepository extends JpaRepository<CompteEntity, Integer>
{

    @Query(name="CompteEntity.findAllWithSolde")
    List<CompteSoldeQueryResult> findAllWithSolde();

    @Query(name="CompteEntity.findExistingTags")
    List<String> findExistingTags();

    @Query(name="CompteEntity.getSoldeByCompteId")
    SoldeQueryResult getSoldeByCompteId(Integer id);

}
