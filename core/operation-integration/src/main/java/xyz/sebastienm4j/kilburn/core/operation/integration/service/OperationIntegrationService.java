package xyz.sebastienm4j.kilburn.core.operation.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRepository;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Service réalisant l'intégration des opérations
 * dans l'application. Il s'agit de la traduction
 * des opérations source en opération, et du passage
 * des règles automatiques s'il y en a.
 */
@Service
public class OperationIntegrationService
{
    private static final Logger logger = LoggerFactory.getLogger(OperationIntegrationService.class);


    @Autowired
    private OperationTraductionService operationTraductionService;

    @Autowired
    private OperationRegleAutoService operationRegleAutoService;

    @Autowired
    private OperationManuelleRapprochementService operationManuelleRapprochementService;

    @Autowired
    private OperationRepository operationRepository;


    public void integrateOperations()
    {
        try
        {
            List<Operation> operations = operationTraductionService.translateOperations();
            if(isEmpty(operations)) {
                return;
            }

            operations = operationRegleAutoService.applyRules(operations);
            if(isEmpty(operations)) {
                return;
            }

            operations = operationManuelleRapprochementService.rapprochementOperations(operations);
            if(isEmpty(operations)) {
                return;
            }

            operationRepository.save(operations.stream().map(o -> (OperationEntity)o).collect(Collectors.toList()));

        } catch(Exception ex) {
            logger.error("Echec de l'intégration, erreur inconnue", ex);
        }
    }

}
