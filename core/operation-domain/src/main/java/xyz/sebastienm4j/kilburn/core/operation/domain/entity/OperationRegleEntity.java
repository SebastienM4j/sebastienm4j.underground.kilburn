package xyz.sebastienm4j.kilburn.core.operation.domain.entity;

import javax.persistence.*;

/**
 * Règle applicable à des opérations.
 */
@Entity
@Table(name="operation_regle")
public class OperationRegleEntity
{
    public static enum Langage
    {
        JAVASCRIPT,
        GROOVY
    }

    private Integer id;
    /** Nom de la classe de la règle implémentée ici */
    private String regle;
    private String libelle;
    private String description;
    /** Indique si la règle est appliquée automatiquement à l'intégration des opérations */
    private boolean auto;
    /** Ordre d'application de la règle s'il y en a plusieurs du même type */
    private Integer ordre;

    private Langage langage;
    /** Implémentation sous la forme d'un script Javascript ou Groovy de la règle */
    private String script;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Column(name="regle", nullable=false)
    public String getRegle()
    {
        return regle;
    }

    public void setRegle(String regle)
    {
        this.regle = regle;
    }

    @Column(name="libelle", length=100, nullable=false)
    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    @Column(name="description")
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Column(name="auto")
    public boolean isAuto()
    {
        return auto;
    }

    public void setAuto(boolean auto)
    {
        this.auto = auto;
    }

    @Column(name="ordre", nullable=false)
    public Integer getOrdre()
    {
        return ordre;
    }

    public void setOrdre(Integer ordre)
    {
        this.ordre = ordre;
    }

    @Column(name="langage", length=10, nullable=false)
    @Enumerated(EnumType.STRING)
    public Langage getLangage()
    {
        return langage;
    }

    public void setLangage(Langage langage)
    {
        this.langage = langage;
    }

    @Column(name="script", nullable=false)
    public String getScript()
    {
        return script;
    }

    public void setScript(String script)
    {
        this.script = script;
    }

}
