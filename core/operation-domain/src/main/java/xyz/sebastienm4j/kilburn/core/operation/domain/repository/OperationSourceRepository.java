package xyz.sebastienm4j.kilburn.core.operation.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationSourceEntity;

import java.util.List;

@Repository
public interface OperationSourceRepository extends JpaRepository<OperationSourceEntity, Long>
{

    long countByDatasourceAndCompteAndData(String datasource, String compte, String data);

    @Query("SELECT os FROM OperationSourceEntity os WHERE os.id NOT IN (SELECT s.id FROM OperationEntity o JOIN o.sources s)")
    List<OperationSourceEntity> findAllNotReferencedByOperation();

}
