package xyz.sebastienm4j.kilburn.core.operation.domain.entity;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="operation")
public class OperationEntity implements Operation
{
    private Long id;
    private Compte compte;

    private Instant date;
    private Instant dateValeur;
    private Mode mode;
    private String tiers;
    private String libelle;
    private String reference;
    private BigDecimal montant;

    private String commentaire;
    private Set<String> tags;
    private boolean pointee;

    private Provenance provenance;
    private Type type;

    private List<OperationSource> sources;


    @Override
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    public Long getId()
    {
        return id;
    }

    @Override
    public void setId(Long id)
    {
        this.id = id;
    }

    @Override
    @ManyToOne(targetEntity=CompteEntity.class, fetch=FetchType.LAZY)
    @JoinColumn(name="compte", referencedColumnName="id", nullable=false)
    public Compte getCompte()
    {
        return compte;
    }

    @Override
    public void setCompte(Compte compte)
    {
        this.compte = (CompteEntity)compte;
    }

    @Override
    @Column(name="date", nullable=false)
    public Instant getDate()
    {
        return date;
    }

    @Override
    public void setDate(Instant date)
    {
        this.date = date;
    }

    @Override
    @Column(name="date_valeur")
    public Instant getDateValeur()
    {
        return dateValeur;
    }

    @Override
    public void setDateValeur(Instant dateValeur)
    {
        this.dateValeur = dateValeur;
    }

    @Override
    @Column(name="mode", length=11)
    @Enumerated(EnumType.STRING)
    public Mode getMode()
    {
        return mode;
    }

    @Override
    public void setMode(Mode mode)
    {
        this.mode = mode;
    }

    @Override
    @Column(name="tiers", length=70)
    public String getTiers()
    {
        return tiers;
    }

    @Override
    public void setTiers(String tiers)
    {
        this.tiers = tiers;
    }

    @Override
    @Column(name="libelle", length=100)
    public String getLibelle()
    {
        return libelle;
    }

    @Override
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    @Override
    @Column(name="reference", length=50)
    public String getReference()
    {
        return reference;
    }

    @Override
    public void setReference(String reference)
    {
        this.reference = reference;
    }

    @Override
    @Column(name="montant", nullable=false)
    public BigDecimal getMontant()
    {
        return montant;
    }

    @Override
    public void setMontant(BigDecimal montant)
    {
        this.montant = montant;
    }

    @Override
    @Column(name="commentaire")
    public String getCommentaire()
    {
        return commentaire;
    }

    @Override
    public void setCommentaire(String commentaire)
    {
        this.commentaire = commentaire;
    }

    @Override
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(
        name="operation_tag",
        joinColumns=@JoinColumn(name="operation", referencedColumnName="id")
    )
    @Column(name="tag", length=50, nullable=false)
    public Set<String> getTags() {
        return tags;
    }

    @Override
    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    @Override
    @Column(name="pointee", nullable=false)
    public boolean isPointee()
    {
        return pointee;
    }

    @Override
    public void setPointee(boolean pointee)
    {
        this.pointee = pointee;
    }

    @Override
    @Column(name="provenance", length=10, nullable=false)
    @Enumerated(EnumType.STRING)
    public Provenance getProvenance()
    {
        return provenance;
    }

    @Override
    public void setProvenance(Provenance provenance)
    {
        this.provenance = provenance;
    }

    @Override
    @Column(name="type", length=10, nullable=false)
    @Enumerated(EnumType.STRING)
    public Type getType()
    {
        return type;
    }

    @Override
    public void setType(Type type)
    {
        this.type = type;
    }


    @Override
    @ManyToMany(targetEntity=OperationSourceEntity.class, fetch=FetchType.LAZY)
    @JoinTable(
        name="operation_operation_source",
        joinColumns=@JoinColumn(name="operation", referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="operation_source", referencedColumnName="id")
    )
    public List<OperationSource> getSources()
    {
        return sources;
    }

    @Override
    public void setSources(List<OperationSource> sources)
    {
        this.sources = sources;
    }
}
