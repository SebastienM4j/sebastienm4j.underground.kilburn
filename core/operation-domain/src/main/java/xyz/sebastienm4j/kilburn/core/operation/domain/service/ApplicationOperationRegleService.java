package xyz.sebastienm4j.kilburn.core.operation.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperation;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperationRegle;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.PackageOperationRegle;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity;

import javax.annotation.PostConstruct;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Service chargé d'appliquer des règles sur des opérations.
 */
@Service
public class ApplicationOperationRegleService
{
    private static final Logger logger = LoggerFactory.getLogger(ApplicationOperationRegleService.class);


    @Autowired
    private OperationDomainMapper operationDomainMapper;

    private ScriptEngineManager scriptEngineManager;

    @PostConstruct
    public void initScriptEngineManager()
    {
        scriptEngineManager = new ScriptEngineManager();
    }


    /**
     * Conditionnement (regroupement ou ventilation) d'opérations.
     *
     * @param regle Règle à appliquer (non <tt>null</tt>, de type <tt>PackageOperationRegle</tt>)
     * @param operations Liste de opérations à traiter (non <tt>null</tt>, non vide)
     * @return Les opérations résultantes
     * @throws ScriptException Erreur d'exécution du script ou de validation du résultat produit
     */
    public List<Operation> packaging(OperationRegleEntity regle, List<Operation> operations) throws ScriptException
    {
        Assert.notNull(regle, "Absence de la règle");
        Assert.isTrue(PackageOperationRegle.class.getName().equals(regle.getRegle()), "Nom de la règle incorrecte");
        Assert.notEmpty(operations, "Liste des opérations null ou vide");

        // Calcul des montants par opération source
        // afin de contrôller que le packaging n'altère
        // pas le solde global, mais aussi le montant
        // par opération

        Map<Long, BigDecimal> montants = operations.stream()
                                                   .collect(Collectors.toMap(o -> o.getSources().get(0).getId(), o -> o.getMontant()));

        // Conditionnement des opérations

        operations = doPackaging(regle, operations);

        if(!checkPackaging(montants, operations)) {
            throw new ScriptException("La règle de conditionnement d'opérations : id ["+regle.getId()+"], libellé ["+regle.getLibelle()+"] (script ["+regle.getLangage().name()+"]) est erronée, les montants ne sont pas conservés");
        }

        return operations;
    }

    private List<Operation> doPackaging(OperationRegleEntity regle, List<Operation> operations) throws ScriptException
    {
        logger.debug("Application de la règle de conditionnement d'opérations : id [{}], libellé [{}] (script [{}])", regle.getId(), regle.getLibelle(), regle.getLangage().name());

        ScriptEngine engine = scriptEngineManager.getEngineByName(regle.getLangage().name().toLowerCase());
        engine.eval(regle.getScript());
        Invocable invocable = (Invocable)engine;
        PackageOperationRegle rule = invocable.getInterface(PackageOperationRegle.class);

        return rule.packaging(operations);
    }

    private boolean checkPackaging(Map<Long, BigDecimal> montants, List<Operation> operations)
    {
        if(isEmpty(operations)) {
            return false;
        }

        Map<Long, BigDecimal> montantsAfter = operations.stream()
                                                        .collect(Collectors.toMap(o -> o.getSources().get(0).getId(), o -> o.getMontant()));


        BigDecimal solde = montants.values().stream().reduce(BigDecimal::add).get();
        BigDecimal soldeAfter = montantsAfter.values().stream().reduce(BigDecimal::add).get();
        if(solde.compareTo(soldeAfter) != 0) {
            return false;
        }

        return montants.entrySet().stream()
                                  .allMatch(e -> e.getValue().compareTo(montantsAfter.get(e.getKey())) == 0);
    }


    /**
     * Modification d'opérations.
     *
     * @param regle Règle à appliquer (non <tt>null</tt>, de type <tt>AmendOperationRegle</tt>)
     * @param operations Liste de opérations à traiter (non <tt>null</tt>, non vide)
     * @return Les opérations résultantes
     * @throws ScriptException Erreur d'exécution du script
     */
    public List<Operation> amend(OperationRegleEntity regle, List<Operation> operations) throws ScriptException
    {
        Assert.notNull(regle, "Absence de la règle");
        Assert.isTrue(AmendOperationRegle.class.getName().equals(regle.getRegle()), "Nom de la règle incorrecte");
        Assert.notEmpty(operations, "Liste des opérations null ou vide");

        logger.debug("Application de la règle de modification d'opérations : id [{}], libellé [{}] (script [{}])", regle.getId(), regle.getLibelle(), regle.getLangage().name());

        ScriptEngine engine = scriptEngineManager.getEngineByName(regle.getLangage().name().toLowerCase());
        engine.eval(regle.getScript());
        Invocable invocable = (Invocable)engine;
        AmendOperationRegle rule = invocable.getInterface(AmendOperationRegle.class);

        operations.forEach(o -> doAmend(rule, o));

        return operations;
    }

    private void doAmend(AmendOperationRegle rule, Operation operation)
    {
        AmendOperation op = operationDomainMapper.map(operation, AmendOperation.class);

        rule.amend(op);

        // On ne prend en compte que les valeurs modifiables

        operation.setDate(op.getDate());
        operation.setDateValeur(op.getDateValeur());
        operation.setMode(op.getMode());
        operation.setTiers(op.getTiers());
        operation.setLibelle(op.getLibelle());
        operation.setReference(op.getReference());
        operation.setCommentaire(op.getCommentaire());
        operation.setTags(op.getTags());
    }

}
