package xyz.sebastienm4j.kilburn.core.operation.domain.service;

import org.springframework.stereotype.Service;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.api.operation.domain.factory.OperationDomainFactory;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationSourceEntity;

@Service
public class DefaultOperationDomainFactory implements OperationDomainFactory
{

    @Override
    public Operation newOperation()
    {
        return new OperationEntity();
    }

    @Override
    public OperationSource newOperationSource()
    {
        return new OperationSourceEntity();
    }

}
