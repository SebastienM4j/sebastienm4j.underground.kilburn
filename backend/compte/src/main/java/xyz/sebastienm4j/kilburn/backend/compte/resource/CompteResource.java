package xyz.sebastienm4j.kilburn.backend.compte.resource;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xyz.sebastienm4j.kilburn.backend.compte.dto.CompteDTO;
import xyz.sebastienm4j.kilburn.backend.compte.dto.CompteSoldeDTO;
import xyz.sebastienm4j.kilburn.backend.compte.dto.SoldeDTO;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;
import xyz.sebastienm4j.kilburn.core.compte.domain.queryresult.CompteSoldeQueryResult;
import xyz.sebastienm4j.kilburn.core.compte.domain.queryresult.SoldeQueryResult;
import xyz.sebastienm4j.kilburn.core.compte.domain.repository.CompteRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRepository;
import xyz.sebastienm4j.kilburn.shared.rest.exception.NotFoundException;
import xyz.sebastienm4j.kilburn.shared.rest.exception.UnprocessableEntityException;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/rest/backend/comptes")
public class CompteResource
{
    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    @Qualifier("backendCompteDTOMapper")
    private MapperFacade mapper;


	@GetMapping
	public List<CompteSoldeDTO> findAllWithSolde()
	{
		List<CompteSoldeQueryResult> comptes = compteRepository.findAllWithSolde();
		return mapper.mapAsList(comptes, CompteSoldeDTO.class);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void addCompte(@RequestBody CompteDTO compteDTO)
	{
	    if(compteDTO.getId() != null) {
	        throw new UnprocessableEntityException();
	    }

	    CompteEntity compte = mapper.map(compteDTO, CompteEntity.class);
	    compteRepository.save(compte);
	}

	@GetMapping("/{id}")
    public CompteDTO getCompte(@PathVariable Integer id)
    {
        CompteEntity compte = compteRepository.findOne(id);
        if(compte == null) {
            throw new NotFoundException();
        }

        return mapper.map(compte, CompteDTO.class);
    }

	@PutMapping("/{id}")
	public void updateCompte(@PathVariable Integer id, @RequestBody CompteDTO compteDTO)
	{
	    if(compteDTO.getId() == null || !compteDTO.getId().equals(id)) {
	        throw new UnprocessableEntityException();
	    }

	    boolean exists = compteRepository.exists(id);
	    if(!exists) {
	        throw new NotFoundException();
	    }

	    CompteEntity compte = mapper.map(compteDTO, CompteEntity.class);
	    compteRepository.save(compte);
	}

	@DeleteMapping("/{id}")
	@Transactional
    public void deleteCompte(@PathVariable Integer id)
    {
	    boolean exists = compteRepository.exists(id);
        if(!exists) {
            throw new NotFoundException();
        }

        operationRepository.deleteByCompteId(id);
        compteRepository.delete(id);
    }

	@GetMapping("/{id}/solde")
	public SoldeDTO getCompteSolde(@PathVariable Integer id)
	{
	    SoldeQueryResult solde = compteRepository.getSoldeByCompteId(id);
	    if(solde == null) {
	        throw new NotFoundException();
	    }

	    return mapper.map(solde, SoldeDTO.class);
	}

	@GetMapping("/tags")
	public List<String> getExistingTags()
	{
	    return compteRepository.findExistingTags();
	}

}
