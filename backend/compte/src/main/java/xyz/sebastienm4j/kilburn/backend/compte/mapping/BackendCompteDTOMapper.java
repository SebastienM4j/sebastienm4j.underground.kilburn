package xyz.sebastienm4j.kilburn.backend.compte.mapping;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import xyz.sebastienm4j.kilburn.backend.compte.dto.CompteDTO;
import xyz.sebastienm4j.kilburn.backend.compte.dto.CompteSoldeDTO;
import xyz.sebastienm4j.kilburn.backend.compte.dto.SoldeDTO;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;
import xyz.sebastienm4j.kilburn.core.compte.domain.queryresult.CompteSoldeQueryResult;
import xyz.sebastienm4j.kilburn.core.compte.domain.queryresult.SoldeQueryResult;

@Component
public class BackendCompteDTOMapper extends ConfigurableMapper
{

    @Override
    protected void configure(MapperFactory factory)
    {
        factory.classMap(CompteEntity.class, CompteDTO.class)
               .byDefault()
               .register();

        factory.classMap(CompteSoldeQueryResult.class, CompteSoldeDTO.class)
               .byDefault()
               .register();


        factory.classMap(SoldeQueryResult.class, SoldeDTO.class)
               .byDefault()
               .register();
    }

}
