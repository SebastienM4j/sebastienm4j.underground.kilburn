package xyz.sebastienm4j.kilburn.backend.operation.dto;

import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity.Langage;

/**
 * DTO pour transporter une règle applicable aux opérations.
 */
public class OperationRegleDTO
{
    private Integer id;
    private String regle;
    private String libelle;
    private String description;
    private boolean auto;
    private Integer ordre;
    private Langage langage;
    private String script;


    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getRegle()
    {
        return regle;
    }

    public void setRegle(String regle)
    {
        this.regle = regle;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isAuto()
    {
        return auto;
    }

    public void setAuto(boolean auto)
    {
        this.auto = auto;
    }

    public Integer getOrdre()
    {
        return ordre;
    }

    public void setOrdre(Integer ordre)
    {
        this.ordre = ordre;
    }

    public Langage getLangage()
    {
        return langage;
    }

    public void setLangage(Langage langage)
    {
        this.langage = langage;
    }

    public String getScript()
    {
        return script;
    }

    public void setScript(String script)
    {
        this.script = script;
    }

}
