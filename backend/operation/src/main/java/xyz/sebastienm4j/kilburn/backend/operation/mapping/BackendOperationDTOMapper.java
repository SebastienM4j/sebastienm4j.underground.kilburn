package xyz.sebastienm4j.kilburn.backend.operation.mapping;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.metadata.Type;
import org.springframework.stereotype.Component;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationRegleDTO;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationRegleListDTO;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationSlimDTO;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class BackendOperationDTOMapper extends ConfigurableMapper
{

    @Override
    protected void configure(MapperFactory factory)
    {
        // FIXME à supprimer quand orika sera mise à jour avec ceci https://github.com/orika-mapper/orika/pull/178
        ConverterFactory converterFactory = factory.getConverterFactory();
        converterFactory.registerConverter(new LocalDateConverter());
        converterFactory.registerConverter(new LocalDateTimeConverter());
        converterFactory.registerConverter(new InstantConverter());

        factory.classMap(OperationEntity.class, OperationSlimDTO.class)
               .byDefault()
               .register();


        factory.classMap(OperationRegleEntity.class, OperationRegleDTO.class)
               .byDefault()
               .register();

        factory.classMap(OperationRegleEntity.class, OperationRegleListDTO.class)
               .byDefault()
               .register();
    }


    protected static class LocalDateConverter extends BidirectionalConverter<LocalDate, LocalDate>
    {
        @Override
        public LocalDate convertTo(LocalDate source, Type<LocalDate> destinationType, MappingContext mappingContext)
        {
            return (LocalDate.from(source));
        }

        @Override
        public LocalDate convertFrom(LocalDate source, Type<LocalDate> destinationType, MappingContext mappingContext)
        {
            return (LocalDate.from(source));
        }
    }

    protected static class LocalDateTimeConverter extends BidirectionalConverter<LocalDateTime, LocalDateTime>
    {
        @Override
        public LocalDateTime convertTo(LocalDateTime source, Type<LocalDateTime> destinationType, MappingContext mappingContext)
        {
            return (LocalDateTime.from(source));
        }

        @Override
        public LocalDateTime convertFrom(LocalDateTime source, Type<LocalDateTime> destinationType, MappingContext mappingContext)
        {
            return (LocalDateTime.from(source));
        }
    }

    protected static class InstantConverter extends BidirectionalConverter<Instant, Instant>
    {
        @Override
        public Instant convertTo(Instant source, Type<Instant> destinationType, MappingContext mappingContext)
        {
            return (Instant.from(source));
        }

        @Override
        public Instant convertFrom(Instant source, Type<Instant> destinationType, MappingContext mappingContext)
        {
            return (Instant.from(source));
        }
    }

}
