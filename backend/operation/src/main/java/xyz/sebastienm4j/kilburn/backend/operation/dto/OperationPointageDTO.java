package xyz.sebastienm4j.kilburn.backend.operation.dto;

public class OperationPointageDTO
{
    private boolean pointee;


    public boolean isPointee() {
        return pointee;
    }

    public void setPointee(boolean pointee) {
        this.pointee = pointee;
    }

}
