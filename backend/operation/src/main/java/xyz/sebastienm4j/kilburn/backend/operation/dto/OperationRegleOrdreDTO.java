package xyz.sebastienm4j.kilburn.backend.operation.dto;

/**
 * DTO pour transporter une demande de changement
 * d'ordre d'une règle d'opération.
 */
public class OperationRegleOrdreDTO
{
    private int position;


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
