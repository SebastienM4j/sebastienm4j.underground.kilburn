Kilburn
=======

Management of personal bank accounts.

Imports banking from a data source, and integrates them with Groovy or Javascript custom rules.

Supported banks for data sources : [CMB](https://www.cmb.fr)... only ! ;-)


### Build status

![GitLab pipeline status](https://gitlab.com/SebastienM4j/kilburn/badges/master/build.svg)


Architecture
------------

### Technical stack

* Build : [Gradle](https://docs.gradle.org/3.1/userguide/userguide.html)
* Front :
    * [TypeScript](https://www.typescriptlang.org/)
    * [Angular 4.1](https://angular.io/)
    * [Bootstrap 4](http://getbootstrap.com/docs/4.0/getting-started/introduction/)
* Back :
    * [Java 8](http://docs.oracle.com/javase/8/)
    * [Spring Boot 1.5](https://projects.spring.io/spring-boot/)
* Persistence :
    * [Spring Data JPA](https://projects.spring.io/spring-data-jpa/)
    * [JPA 2.1](https://docs.oracle.com/javaee/7/tutorial/partpersist.htm#BNBPY)
    * [Postgres 9.6](https://www.postgresql.org/docs/9.6/static/index.html)

### Organization of projects

See PlantUML [diagram](doc/projects-diagram.puml).


Installation
------------

_Basic example for a local installation._

Create the database `createdb -U postgres kilburn` and pass the SQL scripts `./gradlew update -PdbUrl=jdbc:postgresql://localhost:5432/kilburn`.

Build the project `./gradlew clean build`.

Create a directory to host the generated war `mkdir ~/kilburn` and copy it into `cp app/webapp-full-package/build/libs/kilburn-app-webapp-full-package-*.war ~/kilburn/kilburn.war`.

Copy the configuration file `cp app/webapp-full-package/src/main/resources/application-dev.yml ~/kilburn/application-prod.yml` and and customize it.

Run with `cd ~/kilburn ; java -jar kilburn.war --spring.profiles.active=prod` and visit http://localhost:12180/
