package xyz.sebastienm4j.kilburn.shared.jpa.converter;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDate;

@Component
@Converter(autoApply=true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Timestamp>
{

    @Override
    public Timestamp convertToDatabaseColumn(LocalDate attribute)
    {
        return attribute != null ? Timestamp.valueOf(attribute.atStartOfDay()) : null;
    }

    @Override
    public LocalDate convertToEntityAttribute(Timestamp dbData)
    {
        return dbData != null ? dbData.toLocalDateTime().toLocalDate() : null;
    }

}
