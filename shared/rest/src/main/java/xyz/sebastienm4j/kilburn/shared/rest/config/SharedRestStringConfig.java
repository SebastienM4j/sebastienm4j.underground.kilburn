package xyz.sebastienm4j.kilburn.shared.rest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.shared.json.config.JacksonSpringConfig;

@Configuration
@Import(JacksonSpringConfig.class)
@ComponentScan("xyz.sebastienm4j.kilburn.shared.rest.advice")
public class SharedRestStringConfig
{

}
