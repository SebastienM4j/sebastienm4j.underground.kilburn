package xyz.sebastienm4j.kilburn.shared.json.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class LocalDateTimeTimestampDeserializer extends JsonDeserializer<LocalDateTime>
{

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException
    {
        String date = jp.readValueAs(String.class);
        if(!StringUtils.hasText(date)) {
            return null;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(date)), ZoneId.systemDefault());
    }

}
