package xyz.sebastienm4j.kilburn.shared.json.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration Spring pour Jackson.
 */
@Configuration
public class JacksonSpringConfig
{

    /*
     * Configuration du mapper Jackson pour les dates Java 8.
     * Cf https://geowarin.github.io/jsr310-dates-with-jackson.html
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer configureJackson()
    {
        return (builder) -> {
            builder
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .failOnUnknownProperties(false)
                .serializationInclusion(JsonInclude.Include.NON_NULL);
        };
    }

}
