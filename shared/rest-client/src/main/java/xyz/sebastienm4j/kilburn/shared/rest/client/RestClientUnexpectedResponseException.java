package xyz.sebastienm4j.kilburn.shared.rest.client;

import org.springframework.http.HttpStatus;

import java.io.IOException;

/**
 * Signale un code de retour inatendu suite à une requête REST.
 */
public class RestClientUnexpectedResponseException extends IOException
{
    private static final long serialVersionUID = 9175695913292076249L;

    private HttpStatus httpStatus;


    public RestClientUnexpectedResponseException(HttpStatus httpStatus)
    {
        super();
        this.httpStatus = httpStatus;
    }

    public RestClientUnexpectedResponseException(HttpStatus httpStatus, String message)
    {
        super(message);
        this.httpStatus = httpStatus;
    }


    /**
     * @return Code de retour obtenu
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
